#include <iostream>
#include "stdlib.h"
#include "ttt.h"

int main(int argc, char** argv) {
  bool validArgs = true;
  long boardSize = 0;
  if (argc != 2) validArgs = false;
  else {
    char *p;
    errno = 0;
    int conv = strtol(argv[1], &p, 10);
    if (errno != 0 || *p != '\0' || conv > INT_MAX)
      validArgs = false;
    else
      boardSize = conv;
    
    if (boardSize < 0)
      validArgs = false;
  }
  
  if (!validArgs) {
    std::cout << "Tic Tac Toe! Usage: ttt <boardSize>" << std::endl;
    return 1;
  }
  
  TicTacToe game;
  game.newGame(boardSize, TicTacToe::X);
  
  return 0;
}
