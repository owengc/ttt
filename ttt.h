#include <iostream>
#include <sstream>
#include <cassert>
#include <string>
#include "grid.h"

class TicTacToe
{
public:
  enum XO {
    BLANK = 0,
    X = -1,
    O = 1
  };
  
  TicTacToe() :
  m_currentPlayer(BLANK),
  m_board(),
  m_gameOn(false)
  {};
  
  void newGame(unsigned int boardSize, XO firstPlayer)
  {
    if (firstPlayer != X && firstPlayer != O) {
      std::cout << "You must choose if X or O will start the game. Please try again." << std::endl;
      return;
    }
    else if (boardSize == 0) {
      std::cout << "Board size must be greater than zero! Please try again" << std::endl;
      return;
    }
    else if (boardSize > 10) {
      std::cout << "Who wants to play tic tac toe for that long?" << std::endl;
      return;
    }
    else if (boardSize <= 2) {
      std::cout << "This game won't be very fun..." << std::endl;
    }
    
    printNewGame();
    
    m_currentPlayer = firstPlayer;
    m_board.initialize(boardSize);
    m_gameOn = true;
    while (m_gameOn) {
      printBoard();
      printNextTurn();
      int row = 0;
      int col = 0;
      std::string rawInput;
      std::getline(std::cin, rawInput);
      if (rawInput == "q") { // exit the game
        printQuit();
        return;
      }
      else { // parse and validate input
        bool validInput = false;
        std::stringstream stream(rawInput);
        try {
          std::vector<int> input((std::istream_iterator<int>(stream)), (std::istream_iterator<int>()));
          if (input.size() == 2) {
            row = input[0] - 1;
            col = input[1] - 1;
            if (row >= 0 && row < boardSize && col >= 0 && col < boardSize)
              validInput = true;
          }
        }
        catch(std::exception e) {
          // ...
        }
        if (!validInput) {
          printInvalidInput();
          continue;
        }
      }
      
      if (!nextMove(row, col)) {
        printInvalidMove();
        continue;
      }
      
      XO winner = m_board.update(row, col);
      if (m_board.stalemate()) {
        m_currentPlayer = BLANK;
        m_gameOn = false;
      }
      else if (winner == BLANK) {
        m_currentPlayer = XO(-int(m_currentPlayer));
        continue;
      }
      else { // somebody won
        m_currentPlayer = winner;
        m_gameOn = false;
      }
    }
    printBoard();
    printGameOver();
  }
  
private:
  class Board : public Grid<XO>
  {
  private:
    using Grid<XO>::resize; // don't want to expose this
    unsigned int m_size; // board side length
    
    // true means it's still winnable, false means it's been blocked...
    std::vector<bool> rowStates;
    std::vector<bool> colStates;
    std::vector<bool> diagStates; // 0: downward diagonal, 1 : upward diagonal
    
    bool isUpwardDiagonal(unsigned int row, unsigned int col)
    {
      unsigned int index = row * m_nCols + col;
      unsigned int mod = m_size - 1;
      unsigned int last = m_size * m_size - 1;
      return (index % mod == 0 && index != 0 && index != last);
    }
  public:
    void initialize(unsigned int size)
    {
      Grid<XO>::resize(size, size, BLANK);
      rowStates.assign(size, true);
      colStates.assign(size, true);
      diagStates.assign(2, true);
      m_size = size;
    }
    
    unsigned int size()
    {
      return m_size;
    }
    
    // returns outcome of most recent turn: winner X, O or no winner yet (BLANK)
    XO update(unsigned int lastRow, unsigned int lastCol) // coordinates of most recent move
    {
      int lastPlayer = Grid<XO>::at(lastRow, lastCol);
      XO outcome = BLANK;
      bool isDiagDown = (lastRow == lastCol), isDiagUp = isUpwardDiagonal(lastRow, lastCol);
      
      int rowSum = 0, colSum = 0, diagDownSum = 0, diagUpSum = 0; // abs(sum) == m_size if somebody won
      // check if the player won, update state info to save cycles later
      for (int i = 0; i < m_size; i++) {
        // check across if row was winnable
        if (rowStates[lastRow]) {
          int xo = Grid<XO>::at(lastRow, i);
          rowSum += xo;
          if (xo != lastPlayer && xo != BLANK)
            rowStates[lastRow] = false; // both players have pieces on this row, can't win on it
        }
        // check up/down if column was winnable
        if (colStates[lastCol]) {
          int xo = Grid<XO>::at(i, lastCol);
          colSum += xo;
          if (xo != lastPlayer && xo != BLANK)
            colStates[lastCol] = false; // both players have pieces on this column, can't win on it
        }
  
        // check downward diagonal
        if (isDiagDown) {
          if (diagStates[0]) {
            int xo = Grid<XO>::at(i, i);
            diagDownSum += xo;
            if (xo != lastPlayer && xo != BLANK)
              diagStates[0] = false; // both players have pieces on downward diagonal, can't win on it
          }
        }
        
        // check upward diagonal
        if (isDiagUp) {
          if (diagStates[1]) {
            int xo = Grid<XO>::at(m_size - 1 - i, i);
            diagUpSum += xo;
            if (xo != lastPlayer && xo != BLANK)
              diagStates[1] = false; // both players have pieces on upward diagonal, can't win on it
          }
        }
      }
      int winSum = (lastPlayer == X) ? -m_size : m_size;
      if (rowSum == winSum || colSum == winSum || diagDownSum == winSum || diagUpSum == winSum)
        outcome = XO(lastPlayer);
      return outcome;
    }
    
    // determines if neither player can win the game
    bool stalemate()
    {
      bool rowOpen = false, colOpen = false, diagOpen = false;
      for (int i = 0; i < m_size; i++) {
        rowOpen = rowOpen || rowStates[i];
        colOpen = colOpen || colStates[i];
      }
      diagOpen = diagOpen || diagStates[0] || diagStates[1];
      return !(rowOpen || colOpen || diagOpen);
    }
  };
  
  XO m_currentPlayer;
  Board m_board; // Grid class could also be used for proper GUI...
  bool m_gameOn; // Is the game still active
  
  bool nextMove(unsigned int row, unsigned col) {
    // valid move?
    XO& cell = m_board.at(row, col);
    if (cell != BLANK)
      return false;
    else
      cell = m_currentPlayer;
    return true;
  }
  
  const std::string xoAsString(XO xo) const
  {
    switch (xo) {
      case BLANK:
        return " ";
      case X:
        return "X";
      case O:
        return "O";
      default:
        return "?";
    }
  }
  
  void printBoard()
  {
    const unsigned int size = m_board.size();
    for (int i = 0; i < size; i++) {
      for (int j = 0; j < size; j++) {
        std::cout << "[" + xoAsString(m_board.at(i, j)) + "]";
      }
      std::cout << std::endl;
    }
  }
  
  void printNewGame()
  {
    std::cout << "Welcome to Tic Tac Toe! You can quit by entering 'q'." << std::endl;
  }
  
  void printNextTurn()
  {
    std::cout << "Player " << xoAsString(m_currentPlayer)
    << ", your turn! Please enter your move as <row column>." << std::endl;
  }
  
  void printInvalidInput()
  {
    std::cout << "Invalid input! Please enter tile coordinates between 1 and " << m_board.size()
    << " as <row column>." << std::endl;
  }
  
  void printInvalidMove()
  {
    std::cout << "Invalid move! Please choose an open tile." << std::endl;
  }
  
  void printGameOver()
  {
    if (m_currentPlayer == BLANK)
      std::cout << "Game over, stalemate!" << std::endl;
    else
      std::cout << "Game over, player " << xoAsString(m_currentPlayer) << " wins!" << std::endl;
  }
  
  void printQuit()
  {
    std::cout << "Goodbye!" << std::endl;
  }
};
