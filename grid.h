#include <vector>
#include <iostream>
#include <cassert>

template <class T>
class Grid {
protected:
  std::vector<T> m_cells;
  unsigned int m_nRows;
  unsigned int m_nCols;
  
public:
  Grid() :
  m_nRows(0), m_nCols(0)
  {};
  
  Grid(unsigned int numRows, unsigned int numCols)
  {
    resize(numRows, numCols);
  }
  
  void resize(unsigned int numRows, unsigned int numCols, T value)
  {
    m_nRows = numRows;
    m_nCols = numCols;
    unsigned int nCells = m_nRows * m_nCols;
    m_cells.assign(nCells, value);
  }
  
  // rows and columns are zero-indexed...
  T& at(unsigned int row, unsigned int col)
  {
    // translate coordinate to index
    unsigned int index = row * m_nCols + col;
    try {
      return m_cells.at(index);
    }
    catch (std::exception) {
      std::cout << "Invalid row or column index!" << std::endl;
      assert(false); // brutal
    }
  }
  
  const unsigned int numRows() const
  {
    return m_nRows;
  }
  
  const unsigned int numCols() const
  {
    return m_nCols;
  }
  
};
