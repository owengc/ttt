CXX=clang++
CXXFLAGS=-g -std=c++11 -Wall -pedantic
targets=ttt

.PHONY: all
all: $(targets)

ttt: main.o
	$(CXX) $^ -o $@

main.o: main.cpp
	$(CC) $(CXXFLAGS) -c $< -o $@

.PHONY: clean
clean:
	$(RM) $(targets) *.o
